{
  description = "Arion utility functions for personal use";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    pre-commit-hooks = {
      url = "github:cachix/pre-commit-hooks.nix";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        flake-utils.follows = "flake-utils";
      };
    };
  };

  outputs = inputs@{ self, ... }:
    with inputs;
    {
      lib =
        { domain, httpUser, httpPass, puid, pgid, tz ? "Europe/London", stateVersion ? "21.05", locale ? "en_GB.UTF-8" }:
          with nixpkgs.lib;
          let
            serviceDefaults = {
              service = {
                restart = "unless-stopped";
                environment = {
                  TZ = tz;
                  PUID = puid;
                  PGID = pgid;
                };
                tmpfs = mkDefault [
                  "/run"
                  "/tmp:exec,mode=777"
                ]; # low priority as NixOS-based containers will override this
                labels = {
                  "traefik.enable" = mkDefault "false";
                }; # disable træfik ingress by default
              };
              out.service.dns_search = [ "dns.podman" ];
            };

            # Common NixOS-based service options
            nixosServiceDefaults = {
              service = { useHostStore = true; };
              nixos = {
                useSystemd = true;
                configuration = {
                  environment.noXlibs = true;
                  boot = {
                    tmp.useTmpfs = true;
                    isContainer = true;
                  };
                  time.timeZone = tz;
                  i18n.defaultLocale = locale;
                  system.stateVersion = stateVersion;
                  systemd = {
                    oomd.enable = false;
                    services.nscd.serviceConfig.RestrictSUIDSGID = nixpkgs.lib.mkForce false;
                  };
                };
              };
            };
          in
          rec {
            escapeDockerCompose = builtins.replaceStrings [ "$" ] [ "$$" ];

            # Function to generate træfik labels for containers
            mkTraefikLabels =
              { service, port, auth ? true, host ? "${service}.${domain}" }:
              {
                "traefik.enable" = "true";
                "traefik.http.routers.${service}.rule" = "Host(`${host}`)";
                "traefik.http.services.${service}.loadbalancer.server.port" =
                  toString port;
                "traefik.http.routers.${service}.tls.certresolver" = "le";
                "traefik.http.routers.${service}.entrypoints" = "websecure";
                "traefik.http.middlewares.compression.compress" = "true";
                "traefik.http.routers.${service}.middlewares" = "compression";
              } // optionalAttrs auth {
                "traefik.http.routers.${service}.middlewares" = "auth,compression";
                "traefik.http.middlewares.auth.basicauth.users" =
                  "${httpUser}:${escapeDockerCompose httpPass}";
              };

            mkService = cfg: mkMerge [ serviceDefaults cfg ];

            mkNixosService = cfg: mkMerge [ serviceDefaults nixosServiceDefaults cfg ];

            mkLsioService =
              { name
              , branch ? "latest"
              , extraConfig ? { }
              , useVpn ? false
              , publish ? false
              , auth ? true
              , port ? null
              }:
              mkMerge [
                serviceDefaults
                {
                  service = {
                    image = "lscr.io/linuxserver/${name}:${branch}";
                    container_name = name;
                    volumes = [ "${name}:/config" ];
                    labels = mkIf publish (mkTraefikLabels {
                      service = name;
                      inherit port;
                      inherit auth;
                    });
                  };
                }
                (optionalAttrs useVpn {
                  service = {
                    network_mode = "service:gluetun";
                    depends_on = [ "gluetun" ];
                  };
                })
                extraConfig
              ];
          };
    } // flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
      in
      {
        formatter = pkgs.nixpkgs-fmt;

        checks = {
          pre-commit-check = pre-commit-hooks.lib.${system}.run {
            src = ./.;
            hooks = {
              nixpkgs-fmt.enable = true;
            };
          };
        };
      });
}
